#include "../include/graph.h"
#include "../include/algorithms.h"
using namespace std;

vector<int> AGM(int &weight, int &nodes, Graph graph, vector<Edge> (*func)(Graph graph)){
    int lastAdded;
    vector<int>  TSP;
    vector<Edge> AGM=func(graph);
    vector<int>  order;
    nodes=graph.nodes;
    order= DFS(AGM);
    lastAdded=0;
    weight=0;
    for (int j = 0; j < order.size(); ++j) {
        weight += graph.matrix[lastAdded][order[j]];
        TSP.push_back(order[j]+1);
        lastAdded=order[j];
    }
    weight += graph.matrix[TSP[TSP.size()-1]-1][TSP[0]-1];
    return TSP;
}
