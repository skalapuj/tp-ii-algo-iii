#include "../include/graph.h"
#include "../include/algorithms.h"
using namespace std;

vector<int> greedy(int &weight, int &nodes, Graph graph){
    vector<int> TSP;
    vector<int> solution(graph.nodes,-1);
    int         nearestNeighbor=-1, lastAdded=0;
    nodes =graph.nodes;
    /*we look for the nearest neighbor for all nodes and those added to vector in order */
    for(int i=0; i<graph.nodes; i++){
        for(int j=0; j<graph.nodes;j++){
            if(lastAdded!=j && solution[j]==-1){
                if((nearestNeighbor !=-1 && graph.matrix[lastAdded][j]<graph.matrix[lastAdded][nearestNeighbor]) ||  nearestNeighbor ==-1)
                    nearestNeighbor=j;
            }
        }
        solution[lastAdded]=(nearestNeighbor==-1)?0 : nearestNeighbor;
        lastAdded=nearestNeighbor;
        nearestNeighbor=-1;
    }
    lastAdded=0;
    weight=0;
    /*we build the TSP*/
    while(TSP.size() < graph.nodes){
        weight += graph.matrix[lastAdded][solution[lastAdded]];
        TSP.push_back(lastAdded+1);
        lastAdded= solution[lastAdded];
    }
    return TSP;
}
