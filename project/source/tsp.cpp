#include <iostream>
#include <chrono>
#include "../include/algorithms.h"
#include <algorithm>
#include <cstring>

using namespace std;

int main(int argc, char *argv[]){
    char   algorithm[200], debug[200];
    int    weight, nodes, i=0, executions=0;
    Graph  graph;
    double time=0.0;
    vector<int> TSP;
    if(argc<2) {
        cout <<"Ha olvidado indicar el nombre del algoritmo." << endl;
        return 0;
    }
    strcpy(algorithm, argv[1]);
    if(argc == 3)
        strcpy(debug,argv[2]);

    graph= makeMatrixGraph();
    while (executions<40){
        auto start = chrono::steady_clock::now();
        if(!strcmp(algorithm,"Greedy"))
            TSP=greedy(weight, nodes, graph);
        else if(!strcmp(algorithm,"AGM"))
            TSP=AGM(weight, nodes, graph, &Prim);
        else if(!strcmp(algorithm,"AGMPrimCuadratico"))
            TSP=AGM(weight, nodes, graph, &PrimLabo);
        else if(!strcmp(algorithm,"LocalSearch"))
            TSP=localSearch(weight, nodes, graph);
        else if(!strcmp(algorithm,"GreedyLocalSearch"))
            TSP=greedyLocalSearch(weight, nodes, graph);
        else if(!strcmp(algorithm,"TabuSearch"))
            TSP=tabuSearch(weight, nodes, graph);
        else{
            cout <<"Algoritmo incorrecto." << endl;
            return 0;
        }
            auto end = chrono::steady_clock::now();
        time += chrono::duration_cast<chrono::microseconds>(end - start).count() ;
        executions++;
    }
    printResult(weight, nodes, TSP, time/(double)executions,atoi(debug));
    return 0;
}
