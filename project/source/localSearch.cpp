#include "../include/graph.h"
#include "../include/algorithms.h"
using namespace std;

// se queda con la primera solución vecina mejor que la actual
vector<int> localSearch(int &weight, int &nodes, Graph graph) {
    vector<int> TSP = greedy(weight, nodes, graph);
    bool  changed = true;
    // corta cuando no encuentra una solución mejor en la vecindad
    while(changed) {
        changed = false;
        for(int i=0;i<TSP.size()-2 && !changed;i++) {
            for(int j=i+2;j<TSP.size() && !changed;j++) {
                // si j=TSP.size()-1 → j+1=TSP.size() → j+1%TSP.size()=0, (TSP.size()-1,0) es la arista que cierra el ciclo
                int d1 = graph.matrix[TSP[i]-1][TSP[i+1]-1] + graph.matrix[TSP[j]-1][TSP[(j+1)%TSP.size()]-1];
                int d2 = graph.matrix[TSP[i]-1][TSP[j]-1] + graph.matrix[TSP[i+1]-1][TSP[(j+1)%TSP.size()]-1];
                // si se encuentra una solución vecina mejor, se hace el swap y se invierten las aristas intermedias
                if (d1 > d2) {
                    invert(TSP,i+1,j);
                    changed = true;
                }
            }
        }
    }
    weight=targetWeight(TSP, graph);
    return TSP;
}

// en cada paso busca la mejor solución en la vecindad
vector<int> greedyLocalSearch(int &weight, int &nodes, Graph graph) {
    vector<int> TSP = greedy(weight, nodes, graph);
    bool changed = true;
    // corta cuando no encuentra una solución mejor en la vecindad
    while(changed) {
        changed = lowWeightNeightbour(graph, TSP);
    }
    weight=targetWeight(TSP, graph);
    return TSP;
}
