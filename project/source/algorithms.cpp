#include "../include/algorithms.h"
#include <stack>

using namespace std;

vector<Edge> Prim(Graph graph) {
    int numberEdge, row, column, min;
    vector<bool> selected(graph.nodes, false);
    vector<Edge> AGM;
    /* set number of edge to 0*/
    numberEdge = 0;
    /*choose 0th vertex and make it true*/
    selected[0] = true;
    /*For every vertex in the set S, find the all adjacent vertices, calculate the distance from the vertex selected at step 1. if the vertex is already in the set S, discard it otherwise choose another vertex nearest to selected vertex  at step 1.*/
    while (numberEdge < graph.nodes - 1) {
        min = INF;
        row = 0;
        column = 0;
        for (int i = 0; i < graph.nodes; i++) {
            if (selected[i]) {
                for (int j = 0; j < graph.nodes; j++) {
                    if (!selected[j]) {  // not in selected and there is an edge
                        if (min > graph.matrix[i][j]) {
                            min = graph.matrix[i][j];
                            row = i;
                            column = j;
                        }
                    }
                }
            }
        }
        Edge e;
        e.nodeA = row;
        e.nodeB = column;
        e.weight = graph.matrix[row][column];
        AGM.push_back(e);
        selected[column] = true;
        numberEdge++;
    }
    return AGM;
}

vector<int> DFS(vector<Edge> edges, int root) {
    int next = 0, ith, child;
    stack<int> stack;
    vector<int>  pred( edges.size()+1, -1);
    vector<int> orden( edges.size()+1, -1);
    vector<int> inList(edges.size()+1, 0);
    orden[next] = root;
    inList[root] = 1;
    stack.push(root);
    while (!stack.empty()) {
        ith = stack.top();
        child = -1;
        for (int i = 0; i < edges.size(); ++i) {
            if (edges[i].nodeA == ith && !inList[edges[i].nodeB]) {
                child = i;
                pred[edges[i].nodeB] = child;
                next++;
                orden[next] = edges[i].nodeB;
                stack.push(edges[i].nodeB);
                inList[edges[i].nodeB] = 1;
                break;
            }
        }
        if (child == -1) {
            stack.pop();
        }
    }
    return orden;
}

void invert(vector<int> &V, int from, int to) {
    int i = from;
    int j = to;
    while (i < j) {
        V[i] = V[i] + V[j];
        V[j] = V[i] - V[j];
        V[i] = V[i] - V[j];
        i++;
        j--;
    }
}

void printResult(int weight, int nodes, vector<int> TSP, double executeTime, int debug){
    if(debug ==1){
        cout << "Peso:  " << weight << endl;
        cout << "Nodos: " << nodes << endl;
        cout << "Vector: ";
        for(int j = 0; j < TSP.size(); ++j) {
            cout << TSP[j] << " ";
        }
        cout << endl;
    }
    else
        cout << nodes << " " << weight << " " << executeTime << " " << endl;
}

bool lowWeightNeightbour(Graph graph, vector<int> &TSP) {
    bool changed = false;
    pair<int, int> best = make_pair(-1, -1);
    for (int i = 0; i < TSP.size() - 2; i++) {
        for (int j = i + 2; j < TSP.size(); j++) {
            pair<int, int> edge = make_pair(i, j);
            // si j=TSP.size()-1 → j+1=TSP.size() → j+1%TSP.size()=0, (TSP.size()-1,0) es la arista que cierra el ciclo
            int d1 = graph.matrix[TSP[i] - 1][TSP[i + 1] - 1] +
                     graph.matrix[TSP[j] - 1][TSP[(j + 1) % TSP.size()] - 1];
            int d2 = graph.matrix[TSP[i] - 1][TSP[j] - 1] +
                     graph.matrix[TSP[i + 1] - 1][TSP[(j + 1) % TSP.size()] - 1];
            // si la solución vecina es mejor..
            if (d1 > d2) {//) d1 > d2) {
                // si es la primera que se encuentra, se guarda como la mejor
                if (best.first == -1) {
                    best = make_pair(i, j);
                    changed = true;
                } // si ya se había encontrado alguna..
                else {
                    int best_d1 = graph.matrix[TSP[best.first] - 1][TSP[best.first + 1] - 1] +
                                  graph.matrix[TSP[best.second] - 1][TSP[(best.second + 1) % TSP.size()] - 1];
                    int best_d2 = graph.matrix[TSP[best.first] - 1][TSP[best.second] - 1] +
                                  graph.matrix[TSP[best.first + 1] - 1][TSP[(best.second + 1) % TSP.size()] - 1];
                    // si la nueva es mejor, se guarda
                    if (d1 - d2 > best_d1 - best_d2){//d1 - d2 > best_d1 - best_d2) {
                        best = make_pair(i, j);
                        changed = true;
                    }
                }
            }
        }
    }
    if (changed)
        invert(TSP, best.first+1, best.second);
    return changed;
}

bool bestNeightbour(Graph graph, vector<int> &TSP, vector<pair<int, int>> &tabu, int advance, int threshold) {
    bool changed = false;
    pair<int, int> best = make_pair(-1, -1);
    int partialSolution = targetWeight(TSP, graph);
    for (int i = 0; i < TSP.size() - 2; i++) {
        for (int j = i + 2; j < TSP.size(); j++) {
            pair<int, int> edge = make_pair(i, j);
            if (!isTabu(edge, tabu) || (isTabu(edge, tabu) && aspireFunction(edge, graph, partialSolution, TSP, threshold))) {
                if (best.first == -1) {
                    best = make_pair(i, j);
                    changed = true;
                } // si ya se había encontrado alguna..
                else {
                    int best_d1 = graph.matrix[TSP[best.first] - 1][TSP[best.first + 1] - 1] +
                                  graph.matrix[TSP[best.second] - 1][TSP[(best.second + 1) % TSP.size()] - 1];
                    int best_d2 = graph.matrix[TSP[best.first] - 1][TSP[best.second] - 1] +
                                  graph.matrix[TSP[best.first + 1] - 1][TSP[(best.second + 1) % TSP.size()] - 1];
                    int d1 = graph.matrix[TSP[i] - 1][TSP[i + 1] - 1] +
                             graph.matrix[TSP[j] - 1][TSP[(j + 1) % TSP.size()] - 1];
                    int d2 = graph.matrix[TSP[i] - 1][TSP[j] - 1] +
                             graph.matrix[TSP[i + 1] - 1][TSP[(j + 1) % TSP.size()] - 1];
                    // si la nueva es mejor, se guarda
                    if (d1 - d2 > best_d1 - best_d2) {
                        best = make_pair(i, j);
                        changed = true;
                    }
                }
            }
        }
    }
    if (changed)
        invert(TSP, best.first + 1, best.second);
    if (advance != -1)
        tabu[advance % tabu.size()] = best;
    return changed;
}

bool aspireFunction(pair<int, int> edge, Graph graph, int partialSolution, vector<int> solution,  float threshold) {
    int edge1 = graph.matrix[solution[edge.first] - 1][solution[edge.first + 1] - 1] +
               graph.matrix[solution[edge.second] - 1][solution[(edge.second + 1) % solution.size()] - 1];
    int edge2 = graph.matrix[solution[edge.first] - 1][solution[edge.second] - 1] +
             graph.matrix[solution[edge.first + 1] - 1][solution[(edge.second + 1) % solution.size()] - 1];
    return ( partialSolution - edge1 + edge2 < threshold);
}

bool isTabu(pair<int, int> edge,vector<pair<int, int>> tabu){
    if(tabu.empty())
        return false;
    for(int i = 0; i < tabu.size(); ++i) {
        if(tabu[i].first==edge.first && tabu[i].second == edge.second)
            return true;
    }
    return false;
}

int targetWeight(vector<int> TSP, Graph graph){
    int weight=0;
    for (int j = 0; j < TSP.size(); ++j)
        weight += graph.matrix[TSP[j]-1][TSP[(j+1)%TSP.size()]-1];
    return  weight;
}

// O(n²)
vector<Edge> PrimLabo(Graph graph) {
    vector<bool> visitado(graph.nodes, false);
    vector<int> distancia(graph.nodes, INF);
    vector<int> padre(graph.nodes, 0);

    // inicializa las distancias de todos los vértices al 0
    for (int i = 0; i < graph.nodes; ++i) {
        distancia[i] = graph.matrix[0][i];
    }

    visitado[0] = true;

    int i = 0;
    while (i < graph.nodes-1) {
        int v = -1;
        // busca el vértice más cercano al árbol
        for (int j = 1; j < distancia.size(); ++j) {
            if (not visitado[j] and (v == -1 or distancia[j] < distancia[v])) {
                v = j;
            }
        }
        visitado[v] = true;
        // actualiza las distancias
        for (int j = 1; j < graph.nodes; ++j) {
            if (distancia[j] > graph.matrix[v][j] and not visitado[j]) {
                distancia[j] = graph.matrix[v][j];
                padre[j] = v;
            }
        }
        i++;
    }

    vector<Edge> AGM;
    for (int j = 1; j < padre.size(); ++j) {
        Edge e;
        e.nodeA = padre[j];
        e.nodeB = j;
        e.weight = graph.matrix[e.nodeA][e.nodeB];
        AGM.push_back(e);
    }

    return AGM;
}