#include <list>
#include "../include/graph.h"
#include "../include/algorithms.h"
using namespace std;

vector<int> tabuSearch(int &weight, int &nodes, Graph graph){
    vector<int>    s = localSearch(weight, nodes, graph);
    vector<int>    sStar=s;
    vector<int>    sPrime;
    pair<int, int> bottom;
    float          threshold= targetWeight(s, graph)*0.3;
    int            advance=0;
    bottom.first = -1;
    bottom.second= -1;
    vector<pair<int, int>> tabu(10,bottom); //vamos a almacenar las ultimas 10 artistas insertadas
    while(advance < 100){
        // V* son los caminos hamiltonianos que difieren de s en dos aristas
        sPrime=s;
        //busco el mejor de  v*
        if(bestNeightbour(graph, sPrime, tabu, advance,threshold)){
            if(targetWeight(sPrime, graph) < targetWeight(sStar, graph))
                sStar = sPrime;
        }
        s=sPrime;
        advance++;

    }
    weight = targetWeight(sStar, graph);
    return sStar;
}
