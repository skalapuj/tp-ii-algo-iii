#include "../include/graph.h"
using namespace std;

Graph makeGraph(){
	int     n, m, v, w, weight;
    Graph   graph;
    vector<Edge> edges;

    cin >> n;
    cin >> m;
    graph.nodes = n;

    for (int i = 0; i < m; i++)
    {
        Edge edge;

        cin >> v;
        cin >> w;
        cin >> weight;

        edge.nodeA  = v-1;
        edge.nodeB  = w-1;
        edge.weight =weight;

        graph.edges.push_back(edge);
    }
	return graph;
};

Graph makeMatrixGraph(){
    int          n, m, v, w, weight;
    Graph        graph;

    cin >> n >> m;

    vector<vector<int>>   matrix (n,vector<int>(n));

    graph.nodes = n;
    for (int i = 0; i < m; i++)
    {
        Edge edge;

        cin >> v >> w >> weight;

        matrix[v-1][w-1] = weight;
        matrix[w-1][v-1] = weight;
    }

    graph.matrix = matrix;
    return graph;
};

