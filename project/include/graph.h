#ifndef MAP_H
#define MAP_H
#include <vector>
#include <iostream>
using namespace std;

struct Edge {
    int    nodeA;
    int    nodeB;
    float  weight;
};

struct Graph {
    int 		        nodes;
    vector<Edge>        edges; //m
    vector<vector<int>> matrix; //n*n
};

Graph makeGraph();
Graph makeMatrixGraph(); 

#endif


