#include <vector>
#include "../include/graph.h"
#ifndef ALGORITMS_H
#define ALGORITMS_H
#define INF 9999999
using namespace std;
vector<Edge> Prim(Graph graph);
vector<Edge> PrimLabo(Graph graph);
vector<int>  DFS(vector<Edge> edges,  int root=0);
void         invert(vector<int> &V, int from, int to);
void         printResult(int weight, int nodes, vector<int> TSP, double executeTime, int debug);
bool         isTabu(pair<int, int> edge,vector<pair<int, int>> tabu);
bool         aspireFunction(pair<int, int> edge, Graph graph, int partialSolution, vector<int> solution,  float threshold);
int          targetWeight(vector<int> TSP, Graph graph);
bool         lowWeightNeightbour(Graph graph, vector<int> &TSP);
bool         bestNeightbour(Graph graph, vector<int> &TSP, vector<pair<int, int>> &tabu, int advance, int threshold);

vector<int>  AGM(int &weight, int &nodes, Graph graph, vector<Edge> (*func)(Graph graph));
vector<int>  greedy(int &weight, int &nodes, Graph graph);
vector<int>  localSearch(int &weight, int &nodes, Graph graph);
vector<int>  greedyLocalSearch(int &weight, int &nodes, Graph graph);
vector<int>  tabuSearch(int &weight, int &nodes, Graph graph);
#endif


