
import numpy as np
import pandas as pd
import seaborn as sns
import subprocess
import matplotlib.pyplot as plt
import math
import sys


if len(sys.argv) < 3:
    print("Usage: plots.py Family/Algorithm/Benefit/Complexity <List of files to process>") #GreedyLineal GreedyPolinomial GreedyConstante
    exit()

files = {}
for i in range(2, len(sys.argv)):
    files[sys.argv[i]]=(open("./resultados/resultados_%s.txt" % sys.argv[i]))

data=[]
for algorithm in files.keys():
    for line in files[algorithm]:
        result = line.split(' ')
        result[0] = result[0]
        result[1] = result[1]
        result[2] = int(result[2])
        result[3] = int(result[3])
        result[4] = float(result[4]) 
        result.pop()
        data.append(result)

if sys.argv[1] == "Complexity":
    for n in range(2,101):
        result=[]
        result.append("Cota teórica")
        result.append("n³")
        result.append(int(n))
        result.append(int(n))
        result.append(int(n**3)/2)
        data.append(result)

dataF=pd.DataFrame(data,columns=['Algorithm','Family','#Nodes','Max Benefit','Time(μs)'])

if sys.argv[1] == 'Family':
    df = dataF[dataF['#Nodes']<100].copy()
    g = sns.lineplot(x= "#Nodes", y="Time(μs)", data=df, hue="Family" ,palette='Paired')
    #g.set_yscale('log')
    plt.title(dataF['Algorithm'][0])
    plt.ylabel("Time(μs)")
    plt.xlabel("#Nodes")
    #plt.show()
    plt.savefig('./Imagenes/Family/%sFamily.png' % (dataF['Algorithm'][0]))

if sys.argv[1] == 'Algorithm':
    g = sns.lineplot(x= "#Nodes", y="Time(μs)", data=dataF, hue="Algorithm" ,palette='Paired')
    g.set_yscale('log')
    plt.title(dataF['Family'][0])
    plt.ylabel("Time(μs)")
    plt.xlabel("#Nodes")
    #plt.show()
    plt.savefig('./Imagenes/Algorithm/Algorithms%s.png' % (dataF['Family'][0]))

if sys.argv[1] == 'Benefit':
    df = dataF[dataF['#Nodes']%6==0].copy()
    g = sns.barplot(x= "#Nodes", y="Max Benefit", data=df, hue="Algorithm" ,palette='Paired')
    #g.set_yscale('log')
    plt.title(dataF['Family'][0])
    plt.ylabel("Max Benefit")
    plt.xlabel("#Nodes")
    #plt.show()
    plt.savefig('./Imagenes/Benefit/Benefit%s.png' % (dataF['Family'][0]))

if sys.argv[1] == "Complexity":
    df = dataF[dataF['#Nodes']<100].copy()
    g = sns.lineplot(x= "#Nodes", y="Time(μs)", data=df, hue="Family" ,palette='Paired')
    #g.set_yscale('log')
    plt.title(dataF['Algorithm'][0])
    plt.ylabel("Time(μs)")
    plt.xlabel("#Nodes")
    #plt.show()
    plt.savefig('./Imagenes/Complexity/%sComplexity.png' % (dataF['Algorithm'][0]))
