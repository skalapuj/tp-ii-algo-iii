import glob, os
import pandas as pd
import subprocess
from subprocess import Popen, PIPE
import sys

if len(sys.argv) != 3 or sys.argv[1] not in ["Greedy", "AGM","LocalSearch", "GreedyLocalSearch", "TabuSearch", "AGMPrimCuadratico"] or sys.argv[2] not in ["Constante","Lineal","Polinomial","Debug", "TSPLIB"]:
    print("Usage: python3 runAlgorithm.py Greedy/AGM/LocalSearch/GreedyLocalSearch/TabuSearch Constante/Lineal/Polinomial/Debug ")
    exit()

algorithm = sys.argv[1]
typeOfInstance = sys.argv[2] 

os.system("g++ ../source/*.cpp -o ./TSP")

file_name_random = './resultados/resultados_%s%s.txt' % (algorithm, typeOfInstance)

output_file_random = open(file_name_random, 'w+')

os.chdir("./")

for file in glob.glob("./instancias/P-%s/GENERAL-*.csv" % (typeOfInstance)):
    archivo=open(file,'rb')
   
    input_data = archivo.read()
    archivo.close()

    print ("Running instance %s" % (archivo.name))
    command = "cat %s | ./TSP %s" % (archivo.name, algorithm)
    
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    p.wait()
    
    output=p.stdout.read().decode("utf-8")
    output_file_random.write("%s %s %s" % (algorithm, typeOfInstance, output))
    
    print ("Instance %s finished" % (archivo.name))

output_file_random.close()

