#!/bin/bash

for al in "AGM" "AGMPrimCuadratico"; do
	for flia in "Constante" "Lineal" "Polinomial"; do
		python3 ./runAlgorithm.py $al $flia
	done
done
